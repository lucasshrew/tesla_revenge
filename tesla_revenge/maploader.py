import os
from xml.dom.minidom import parse, parseString

import data
from collections import namedtuple
from pygame import Rect

DEBUG = False

def log(*args, **kwargs):
    if DEBUG:
        print(args)

CollRect = namedtuple('CollRect', 'x, y, w, h, type')
SpawnPoint = namedtuple('SpawnPoint', 'x, y, w, h, kind')
Gfx = namedtuple('Gfx', 'x, y, w, h, filename')

class LevelDefinition(object):
    HERO_ENTITY = 'Tesla'
    BG_NAME = 'background'

    def __init__(self, width, height):
        self.background = []
        self.decorations = []
        self.spawn_points = []
        self.hero_spawn = None
        self.collision_rects = []

    def __str__(self):
        s = str(self.background)
        s += str(self.decorations)
        s += str(self.spawn_points)
        s += str(self.hero_spawn)
        s += str(self.collision_rects)
        return s

    def add_background(self, bg):
        self.background.append(bg)

    def add_decoration(self, element):
        self.decorations.append(element)

    def add_spawn_point(self, point):
        if point.kind == self.HERO_ENTITY:
            self.hero_spawn = point
        else:
            self.spawn_points.append(point)

    def add_collision_rect(self, rect):
        r = Rect(rect.x, rect.y, rect.w, rect.h)
        self.collision_rects.append(r)


class LevelParser(object):
    
    def __init__(self, filename):
        self.filename = filename
        self.level = None

    def parse(self):
        dom = parse(data.filepath(self.filename))
        
        root = dom.getElementsByTagName('svg')[0]
        layers = root.getElementsByTagName('g')

        width = root.getAttribute('width')
        height = root.getAttribute('height')

        width = int(width.strip('px'))
        height = int(height.strip('px'))
        self.w = width
        self.h = height

        self.level = LevelDefinition(width, height)

        for layer in layers:
            self.parse_layer(layer)

        return self.level


    def parse_layer(self, layer):
        label = layer.getAttribute('inkscape:label')
        kind, name = label.split(':') 
        
        if kind == "physics":
            self.parse_collision_rects(layer)
        elif kind == "gfx":
            self.parse_gfxs(layer, name)
        elif kind == "logic":
            if name == 'spawn':
                self.parse_spawn_points(layer)
            else:
                log('unknown logic type: ', label)
        else:
            log('unknown layer type: ', label)


    def parse_collision_rects(self, layer):
        for child in layer.childNodes:
            if child.nodeName == 'rect':
                rect = self.parse_collrect(child)
                self.level.add_collision_rect(rect)
            else:
                log('unknon node type: ', child)

            
    def parse_collrect(self, node):
        width = int(float(node.getAttribute('width')))
        height = int(float(node.getAttribute('height')))

        x = int(float(node.getAttribute('x')))
        y = int(float(node.getAttribute('y')))
        
        return CollRect(x, y, width, height, type)

    
    def parse_gfxs(self, layer, name=''):
        for child in layer.childNodes:
            if child.nodeName == 'image':
                gfx = self.parse_gfx(child)
                if self.level.BG_NAME == name:
                    self.level.add_background(gfx)
                else:
                    self.level.add_decoration(gfx)
            else:
                log('unknon node type: ', child)

            
    def parse_gfx(self, node):
        width = int(float(node.getAttribute('width')))
        height = int(float(node.getAttribute('height')))

        x = int(float(node.getAttribute('x')))
        y = int(float(node.getAttribute('y')))

        url = node.getAttribute('xlink:href')
        i = url.rfind('\\')
        if i != -1:
            filename = url[i+1:]
        else:
            filename = os.path.split(url)[1]

        return Gfx(x, y, width, height, filename)


    def parse_spawn_points(self, layer):
        for child in layer.childNodes:
            if child.nodeName == 'rect':
                spoint = self.parse_spawn_point(child)
                self.level.add_spawn_point(spoint)
            else:
                log('unknon node type: ', child)

            
    def parse_spawn_point(self, node):
        label = node.getAttribute('inkscape:label')
        _, kind = label.split(':')

        width = int(float(node.getAttribute('width')))
        height = int(float(node.getAttribute('height')))

        x = int(float(node.getAttribute('x')))
        y = int(float(node.getAttribute('y')))

        return SpawnPoint(x, y, width, height, kind)


if __name__ == '__main__':
    lp = LevelParser('level1.svg')
    l = lp.parse()
    print l


