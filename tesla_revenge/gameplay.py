import maploader
from entities import *


class PlayerState:
    STAND = 0
    WALK = 1
    DECELERATE = 2
    JUMP = 3
    FALL = 4
    ALL = [STAND, WALK, DECELERATE, JUMP, FALL]
    ALL_FLOOR = [STAND, WALK, DECELERATE]
    ALL_AIR = [JUMP, FALL]


class PlayerCommand:
    STAND = 0
    ACCEL_UP = 1
    ACCEL_DOWN = 2
    ACCEL_LEFT = 3
    ACCEL_RIGHT = 4
    DECELERATE = 5


class Direction:
    UP = 1
    DOWN = 2
    LEFT = 3
    RIGHT = 4


class TeslaController(Controller):
    
    def __init__(self, level):
        super(TeslaController, self).__init__()
        self.level = level
    
    def update(self, entity, dt):
        
        commands = []
        
        if self.key_pressed(K_UP):
            commands.append(PlayerCommand.ACCEL_UP)
        else:
            if entity.get_state() == PlayerState.JUMP:
                entity.set_state(PlayerState.FALL)
        
        if self.key_pressed(K_LEFT):
            commands.append(PlayerCommand.ACCEL_LEFT)
        elif self.key_pressed(K_RIGHT):
            commands.append(PlayerCommand.ACCEL_RIGHT)
        else:
            commands.append(PlayerCommand.DECELERATE)
        
        if not commands and entity.speedh == 0:
            commands.append(PlayerCommand.STAND)
        
        return commands


class Tesla(Actor):
    
    def __init__(self, controller, x=0, y=0):
        
        super(Tesla, self).__init__(controller, x, y)
        
        self.add_anim(Anim.get_anim('JAULA_BACK_ROTATE_LEFT'))
        self.add_anim(Anim.get_anim('JAULA_BACK_ROTATE_RIGHT'))
        self.add_anim(Anim.get_anim('TESLA_RUN_LEFT'))
        self.add_anim(Anim.get_anim('TESLA_RUN_RIGHT'))
        self.add_anim(Anim.get_anim('JAULA_FRONT_ROTATE_LEFT'))
        self.add_anim(Anim.get_anim('JAULA_FRONT_ROTATE_RIGHT'))
        self.add_anim(Anim.get_anim('RAYOS_ANIM'))
        
        self.define_state_flow(PlayerState.ALL_FLOOR, PlayerCommand.ACCEL_RIGHT, PlayerState.WALK, dict(dir=Direction.RIGHT))
        self.define_state_flow(PlayerState.ALL_FLOOR, PlayerCommand.ACCEL_LEFT, PlayerState.WALK, dict(dir=Direction.LEFT))
        self.define_state_flow(PlayerState.ALL_FLOOR, PlayerCommand.ACCEL_UP, PlayerState.JUMP)
        self.define_state_flow(PlayerState.ALL_FLOOR, PlayerCommand.DECELERATE, PlayerState.DECELERATE)
        self.define_state_flow(PlayerState.ALL_FLOOR, PlayerCommand.STAND, PlayerState.STAND)
        
        self.maxspeedh = 8
        self.accelh = 0.5
        self.decelh = 0.3
        self.accelv = 0.3
        self.decelv = 0.8
        self.speedh = 0
        self.speedv = 0
        self.dir = Direction.RIGHT
        
        self.set_state(PlayerState.WALK, dict(dir=Direction.RIGHT))
        self.get_anim(6).play().show()
    
    def update_state(self, state, dt):
        
        if state == PlayerState.WALK:
            if self.dir == Direction.LEFT:
                self.speedh -= self.accelh
            elif self.dir == Direction.RIGHT:
                self.speedh += self.accelh
            if abs(self.speedh) > self.maxspeedh:
                self.speedh = self.maxspeedh * cmp(self.speedh, 0)
        
        elif state == PlayerState.JUMP:
            self.speedv += (self.decelv - self.accelv)
        
        elif state == PlayerState.FALL:
            self.speedv += self.decelv
        
        elif state == PlayerState.DECELERATE:
            if self.dir == Direction.LEFT:
                self.speedh += self.decelh
                if self.speedh > 0:
                    self.speedh = 0
            elif self.dir == Direction.RIGHT:
                self.speedh -= self.decelh
                if self.speedh < 0:
                    self.speedh = 0
        
        self.translate(self.speedh, self.speedv)
        
        # Fake physics hack
        if self.y > 520:
            self.y = 520
            self.speedv = 0
            self.set_state(PlayerState.DECELERATE)
        
    def enter_state(self, state, prev_state, params=None):
        
        params = params or dict()
        
        prev_dir = self.dir
        self.dir = params.get('dir', prev_dir)
        
        if state == PlayerState.JUMP:
            self.speedv = -12
        
        if self.dir == Direction.LEFT:
            self.get_anim(0).play().show()
            self.get_anim(2).play().show()
            self.get_anim(4).play().show()
        elif self.dir == Direction.RIGHT:
            self.get_anim(1).play().show()
            self.get_anim(3).play().show()
            self.get_anim(5).play().show()
        
    def exit_state(self, state, next_state, params=None):
        
        if self.dir == Direction.LEFT:
            self.get_anim(0).pause().hide()
            self.get_anim(2).pause().hide()
            self.get_anim(4).pause().hide()
        elif self.dir == Direction.RIGHT:
            self.get_anim(1).pause().hide()
            self.get_anim(3).pause().hide()
            self.get_anim(5).pause().hide()


class Game(object):
    
    def __init__(self):
        
        FrameSet.load_framesets('frames.txt')
        Anim.load_anims('anims.txt')
        
        self.intro = Intro()
        self.level = Level()
    
    def update(self, dt):
        if self.intro.get_state() < IntroState.OVER:
            self.intro.update(dt)
        else:
            self.level.update(dt)
    
    def draw(self, screen):
        if self.intro.get_state() < IntroState.OVER:
            self.intro.draw(screen.surface)
        else:
            self.level.draw(screen.surface)


class LevelState:
    NONE = 0
    INITED = 1
    RUNNING = 2
    WIN = 3
    OVER = 4
    NEXT = 5


class LevelCommand:
    LOAD = 1
    RUN = 2
    FINISH = 3
    FAIL = 4
    ADVANCE = 5
    QUIT = 6


LEVELS = ['level01.svg', 'level02.svg']


class LevelController(Controller):
    
    def update(self, level, dt):
        
        commands = []
        if level.get_state() in (LevelState.NONE, LevelState.NEXT):
            commands.append(LevelCommand.LOAD)
        
        elif level.get_state() == LevelState.INITED:
            if self.any_key_pressed():
                commands.append(LevelCommand.RUN)

        return commands


class Level(Actor):
    
    def __init__(self, level_number=0):
        super(Level, self).__init__(LevelController())

        self.level_def = None
        self.level_number = level_number

        self.decoration_group = EntityGroup()
        self.background_group = EntityGroup()
        self.enemies_group = EntityGroup()

        self.spawn_points = []
        self.coll_map = []

        self.player_group = EntityGroup()
        self.tesla = None

        self.set_state(LevelState.NONE)
        
        self.define_state_flow(LevelState.NONE, LevelCommand.LOAD, LevelState.INITED)
        self.define_state_flow(LevelState.INITED, LevelCommand.RUN, LevelState.RUNNING)
        self.define_state_flow(LevelState.RUNNING, LevelCommand.FINISH, LevelState.WIN)

        self.define_state_flow(LevelState.WIN, LevelCommand.ADVANCE, LevelState.NEXT)
        self.define_state_flow(LevelState.NEXT, LevelCommand.LOAD, LevelState.INITED)
        
        self.define_state_flow(LevelState.RUNNING, LevelCommand.FAIL, LevelState.OVER)
        self.define_state_flow(LevelState.OVER, LevelCommand.ADVANCE, LevelState.NONE)
    
    def load_entities(self):
        self.load_backgrounds()
        self.load_decorations()
        self.load_spawn_points()
        self.load_coll_map()
        self.load_tesla()

    def load_backgrounds(self):
        for b in self.level_def.background:
            print b.x, b.y
            print b
            layer = Layer.create_from_file(b.filename, b.x, b.y, b.w, b.h)
            self.background_group.add(layer)
    
    def load_decorations(self):
        for d in self.level_def.decorations:
            layer = Layer.create_from_file(d.filename, d.x, d.y, d.w, d.h)
            self.decoration_group.add(layer)
    
    def load_tesla(self):
        hs = self.level_def.hero_spawn
        r = Rect(hs.x, hs.y, hs.w, hs.h)
        self.tesla = Tesla(TeslaController(self), *r.center)
        self.player_group.add(self.tesla)

    def load_spawn_points(self):
        self.spawn_points = self.level_def.spawn_points

    def load_coll_map(self):
        self.coll_map = self.level_def.collision_rects

    def update(self, dt):
        super(Level, self).update(dt)

        self.background_group.update(dt)
        self.decoration_group.update(dt)
        self.enemies_group.update(dt)
        self.player_group.update(dt)

    def draw(self, surface, x=0, y=0):
        super(Level, self).draw(surface, x, y)

        self.background_group.draw(surface, x, y)
        self.decoration_group.draw(surface, x, y)
        self.enemies_group.draw(surface, x, y)
        self.player_group.draw(surface, x, y)

        self.debug_draw_coll_map(surface)

    def debug_draw_coll_map(self, surface):
        for r in self.coll_map:
            pygame.draw.rect(surface, (255, 0, 0), r)

    def enter_state(self, state, prev_state, params=None):

        if state == LevelState.NONE or state == LevelState.NEXT:
            self.level_def = maploader.LevelParser(LEVELS[self.level_number]).parse()
        
        elif state == LevelState.INITED:
            self.load_entities()

    def update_state(self, state, dt):
        pass

    def exit_state(self, state, next_state, params=None):
        pass


class IntroState:
    FRAME_00 = 0
    FRAME_01 = 1
    FRAME_02 = 2
    FRAME_03 = 3
    FRAME_04 = 4
    FRAME_05 = 5
    FRAME_06 = 6
    FRAME_07 = 7
    OVER = 8


class IntroCommand:
    NEXT_FRAME = 1


class IntroController(Controller):
    
    def __init__(self):
        self.dt = 0
    
    def update(self, intro, dt):
        
        if intro.get_state() == IntroState.OVER:
            return None
        
        commands = []
        self.dt += dt
        if self.dt >= 3000 or self.any_key_pressed():
            self.dt = 0
            commands.append(IntroCommand.NEXT_FRAME)
        
        return commands


class Intro(Actor):
    
    def __init__(self):
        
        super(Intro, self).__init__(IntroController())
        
        self.add_anim(Anim.get_anim('INTRO_00'), 400, 300)
        self.add_anim(Anim.get_anim('INTRO_01'), 400, 300)
        self.add_anim(Anim.get_anim('INTRO_02'), 400, 300)
        self.add_anim(Anim.get_anim('INTRO_03'), 400, 300)
        self.add_anim(Anim.get_anim('INTRO_04'), 400, 300)
        self.add_anim(Anim.get_anim('INTRO_05'), 400, 300)
        self.add_anim(Anim.get_anim('INTRO_06'), 400, 300)
        self.add_anim(Anim.get_anim('INTRO_07'), 400, 300)
        
        self.define_state_flow(IntroState.FRAME_00, IntroCommand.NEXT_FRAME, IntroState.FRAME_01)
        self.define_state_flow(IntroState.FRAME_01, IntroCommand.NEXT_FRAME, IntroState.FRAME_02)
        self.define_state_flow(IntroState.FRAME_02, IntroCommand.NEXT_FRAME, IntroState.FRAME_03)
        self.define_state_flow(IntroState.FRAME_03, IntroCommand.NEXT_FRAME, IntroState.FRAME_04)
        self.define_state_flow(IntroState.FRAME_04, IntroCommand.NEXT_FRAME, IntroState.FRAME_05)
        self.define_state_flow(IntroState.FRAME_05, IntroCommand.NEXT_FRAME, IntroState.FRAME_06)
        self.define_state_flow(IntroState.FRAME_06, IntroCommand.NEXT_FRAME, IntroState.FRAME_07)
        self.define_state_flow(IntroState.FRAME_07, IntroCommand.NEXT_FRAME, IntroState.OVER)
        
        self.set_state(IntroState.FRAME_00)
    
    def update_state(self, state, dt):
        pass
        
    def enter_state(self, state, prev_state, params=None):
        
        if state < IntroState.OVER:
            self.get_anim(state).play().show()
        
    def exit_state(self, state, next_state, params=None):
        
        if state < IntroState.OVER:
            self.get_anim(state).stop().hide()
