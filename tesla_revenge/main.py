import sys

import pygame, time, random, math, os
from pygame.locals import *

import data
from gameplay import Game, Controller


class Screen(object):
    
    width = 800
    height = 600
    
    def __init__(self):
        screensize = (self.width, self.height)
        self.surface = pygame.display.set_mode(screensize, pygame.DOUBLEBUF)
    
    def clear(self):
        self.surface.fill((180, 180, 180))


def main():
    pygame.init()
    pygame.display.set_caption("Tesla Revenge")
    game = Game()
    screen = Screen()
    run(game, screen)


def run(game, screen):
    clock = pygame.time.Clock()
    while True:
        check_quit()
        Controller.update_keyboard()
        update(game, clock.get_time())
        draw(game, screen)
        clock.tick(30)


def check_quit():
    pressed = pygame.key.get_pressed()
    if pressed[K_ESCAPE] or pygame.event.get(QUIT):
            exit_game()


def update(game, dt):
    if dt > 200: dt = 33
    game.update(dt)


def draw(game, screen):
    screen.clear()
    game.draw(screen)
    pygame.display.flip()


def exit_game():
    pygame.quit()
    sys.exit(0)
