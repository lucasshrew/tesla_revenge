'''

Creation: May 11, 2012
Author: Sebastian Lucas
License: Creative Commons-Attribution-ShakeAlike 3.0

This file is part of the Tesla Revenge project for the PyWeek contest.

You can read the full license here:
http://creativecommons.org/licenses/by-sa/3.0/us/

'''

import os
from xml.dom.minidom import parse

from data import filepath

_verbose = True

class Director( object ):
    def __init__( self ):
        doc = parse( filepath( 'boot.svg' ) )

        svg = doc.getElementsByTagName( 'svg' )
        sw = int( svg[0].getAttribute( 'width' ) )
        sh = int( svg[0].getAttribute( 'height' ) )

        if _verbose:
            print '\t\tViewport set as:', sw, sh

        states = {}
        events = {}
        statesUnique = {}
        eventsUnique = {}

        finalSMFlow = []

        exportVars = {}
        for text in doc.getElementsByTagName('text'):
            for span in text.getElementsByTagName('tspan'):
                for child in span.childNodes:
                    name = child.nodeValue
                    if name.find('=') != -1:
                        var = name.split('=')
                        exportVars[ var[0] ] = var[1]

        functions = {}

        for group in doc.getElementsByTagName('g'):
            id = group.getAttribute('id')
            
            actionToExecute = ''
            for desc in group.getElementsByTagName( 'desc' ):
                actionToExecute += desc.childNodes[0].nodeValue
            
            for text in group.getElementsByTagName('text'):
                for textspan in text.getElementsByTagName('tspan'):
                    for child in textspan.childNodes:
                        name = child.nodeValue
                        if name.find('State_') != -1:
                            states[id] = name
                            statesUnique[name] = id
                            functions[ name ] = actionToExecute
                            actionToExecute = ''
                        elif name.find('Event_') != -1:
                            events[id] = name
                            eventsUnique[name] = id
                            if len( actionToExecute ) > 0:
                                functions[ id ] = actionToExecute
                            actionToExecute = ''

        if _verbose:
            print '\t\tNumber of states:', len(states)
            print '\t\tNumber of events:', len(events)

        smFlow = {}
        i = 'A'

        for path in doc.getElementsByTagName('path'):
            if not path.hasAttribute('inkscape:connection-start'):
                continue
            if not path.hasAttribute('inkscape:connection-end'):
                continue
            start = path.getAttribute('inkscape:connection-start')[1:]
            end = path.getAttribute('inkscape:connection-end')[1:]
            startName = ''
            endName = ''

            for stateName in states:
                if stateName == start:
                    startName = states[stateName]
                if stateName == end:
                    endName = states[stateName]
            for eventName in events:
                if eventName == start:
                    startName = events[eventName]
                if eventName == end:
                    endName = events[eventName]

            smFlow[ startName + ',' + start + ',' + i ] = endName + ',' + end + ',' + i
            i += 'A'

        for key in smFlow:
            if key.find('State_') == -1:
                continue
            startName = key.split(',')[0]
            name, id, dummy = smFlow[key].split(',')
            found = False
            for _key in smFlow:
                _name, _id, dummy = _key.split(',')
                if name == _name and id == _id:
                    if type(smFlow[key]) != dict:
                        smFlow[key] = {}
                    smFlow[key][_key] = smFlow[_key]
                    funcId = None
                    if id in functions:
                        funcId = id
                    finalSMFlow.append( [ startName, _name, smFlow[_key].split(',')[0], funcId ] )
                    found = True
            if not found:
                finalSMFlow.append( [ startName, smFlow[key].split(',')[0], startName, None ] )

        flowFuncs = {}
        flowDict = {}
        for i in finalSMFlow:
            if not i[0] in flowDict:
                flowDict[ i[0] ] = {}
            flowDict[ i[0] ][ i[1] ] = i[2]
            
            if not i[0] in flowFuncs:
                flowFuncs[ i[0] ] = {}
            if not i[1] in flowFuncs[ i[0] ]:
                flowFuncs[ i[0] ][ i[1] ] = {}
            if i[3] == None:
                flowFuncs[ i[0] ][ i[1] ][ i[2] ] = 'None'
            else:
                flowFuncs[ i[0] ][ i[1] ][ i[2] ] = '%s' % ( i[3] )
            if _verbose:
                print '\t\t', i
        
        self._flowDict = flowDict #flow that points out the function name to execute
        self._flowFuncs = flowFuncs #functions name according to flow
        self._functions = functions #a collection of functions

        self._gs = 'State_None'
        self.SetState( 'State_Initialize' )
    
    def SetState( self, state ):
        if state != self._gs:
            if state in self._functions:
                exec( self._functions[ state ], globals(), locals() )
            self._gs = state

    def SendEvent( self, event ):
        if event in self._flowDict[ self._gs ]:
            endState = self._flowDict[ self._gs ][ event ]

            funcId = self._flowFuncs[ self._gs ][ event ][ endState ]
            
            if funcId in self._functions:
                exec( self._functions[ funcId ], globals(), locals() )
            
            self.SetState( endState )


if __name__ == "__main__":
    d = Director()
    d.SendEvent( 'Event_Advance' )
