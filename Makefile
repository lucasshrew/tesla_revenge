#fake Makefile for tesla_revenge, to support the common
# ./configure;make;make install

PYTHON = python

#build: Setup setup.py
build: setup.py
	$(PYTHON) setup.py build


mac: setup.py
	$(PYTHON) setup.py py2app
	ditto --rsrc --arch i386 dist/tesla_revenge.app dist/tesla_revenge.app.i386
	rm -r dist/tesla_revenge.app
	mv dist/tesla_revenge.app.i386 dist/tesla_revenge.app


#install: Setup setup.py
install: setup.py
	$(PYTHON) setup.py install

clean:
	rm -rf build dist MANIFEST .coverage
	rm -f tesla_revenge/*~
	rm -rf bin develop-eggs eggs parts .installed.cfg tesla_revenge.egg-info
	find . -name *.pyc -exec rm {} \;
	find . -name *.swp -exec rm {} \;
	$(PYTHON) setup.py clean

#upload to pypi
upload:
	make clean
	#if you have your gpg key set up... sign your release.
	#$(PYTHON) setup.py sdist upload --sign --identity="Your Name <youremail@example.com>" 
	$(PYTHON) setup.py sdist upload

sdist:
	make clean
	$(PYTHON) setup.py sdist

